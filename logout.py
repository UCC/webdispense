#!/usr/bin/python

import commands,re

print "Content-type: text/json"
print
import cgi,cgitb
cgitb.enable ()
import os
import ldap
import sys

#import getpass
 
result = {}
form = cgi.FieldStorage ()

def checkdata ():
	if not (form.has_key("username") and form.has_key("sid")):
        	return {"result" : "Missing information!"}
        expr_name = re.compile ("^\w*$")
        username = form["username"].value.lower ()
        sid = form["sid"].value
        if (not expr_name.match (username)):
                return {"result" : "Invalid login!"}
	file = open("sessionids", "r")
	output = []
	success = False
	for line in file:
		if (":" + username + ":" + sid + "\n") in line:
			success = True
		else:
			output.append(line)
	file.close()
	if not success:
		log = "Authentication failure for user " + username + " from " + cgi.escape(os.environ["REMOTE_ADDR"]) + "\n"
                syslog.syslog((syslog.LOG_NOTICE | syslog.LOG_AUTH), log)
                #logfile = open("auth.log", "a")
                #logfile.write(log)
                #logfile.close()
                return {"result" : "Authentication Failed"}
	file = open("sessionids", "w")
	file.writelines(output)
	file.close()
	return {"result" : "success"}

print str(checkdata ())
