webdispense
===========

Web interface for Dispense

[BOB] 20100501
How to make the pictures work in the dispense interface:
-there are mappings from slot names to file names in the index.html, just upload a picture for it into the icons directory and add a mapping
-the pictures are 130 pixels wide, 49 high
-don't forget to make a 'dark-' version of the image for when the slot is empty - there is a black50alpha layer in the folder that you can use

Example Crontab
---------------

This clears the sessions

* * * * * cd /services/webdispense/ && /usr/bin/python /services/webdispense/purge.py

Apache Config
-------------

Example config

<VirtualHost *:80>
    Redirect / https://dispense.ucc.asn.au/
    ServerName dispense.ucc.asn.au
    Serveralias dispense.ucc.gu.uwa.edu.au
</virtualhost>

<VirtualHost *:443>
    ServerName dispense.ucc.asn.au
    ServerAlias dispense.ucc.gu.uwa.edu.au
    
    ServerAdmin wheel@ucc.gu.uwa.edu.au

    SSLCertificateKeyFile /etc/letsencrypt/live/wildcard.ucc/privkey.pem
    SSLCertificateFile /etc/letsencrypt/live/wildcard.ucc/fullchain.pem

    Alias /favicon.ico /services/wiki/htdocs/favicon.ico

    # Rewrite urls
    RewriteEngine On

    Alias / /services/webdispense/
    Alias /dispense /services/webdispense/

    # Dispense WebUI
    <Directory /services/webdispense>
      #Options +ExecCGI -Indexes
      Options +ExecCGI +Indexes
      Order deny,allow
      Allow from all
    </Directory>
</VirtualHost>
