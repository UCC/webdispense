#!/usr/bin/python

import commands,re

print "Content-type: text/json"
print
import cgi,cgitb
cgitb.enable ()
import os
import ldap
import string, random
import sys, time, syslog

#import getpass

result = {}
form = cgi.FieldStorage ()

def sid_generator(size=64, chars=string.ascii_uppercase + string.ascii_lowercase + string.digits):
	return ''.join(random.choice(chars) for c in range(size))

def checkdata ():
	expr_name = re.compile ("^\w*$")	
	username = form["username"].value.lower()
	if (not expr_name.match (username)):
		return {"result" : "Invalid login!"}
	passwd = form["passwd"].value
	#dn = "uid=%s,ou=People,dc=ucc,dc=gu,dc=uwa,dc=edu,dc=au" % username
	dn = "cn=%s,cn=Users,dc=ad,dc=ucc,dc=gu,dc=uwa,dc=edu,dc=au" % username
	try:
		ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT,ldap.OPT_X_TLS_NEVER)
		l = ldap.initialize("ldaps://ad.ucc.gu.uwa.edu.au")
		l.simple_bind_s(dn, passwd)
	except:
		log = "Authentication failure for user " + username + " from " + cgi.escape(os.environ["REMOTE_ADDR"]) + "\n"
                syslog.syslog((syslog.LOG_NOTICE | syslog.LOG_AUTH), log)
                #logfile = open("auth.log", "a")
                #logfile.write(log)
                #logfile.close()
		return {"result" : "Authentication Failed"}
	file = open("sessionids", "r")
	output = []
	for line in file:
		if not (":" + username + ":") in line:
			output.append(line)
	file.close()
	sid = sid_generator()
	output.append(str(int(time.time())) + ":" + username + ":" + sid + "\n")
	file = open("sessionids", "w")
	file.writelines(output)
	file.close()
	return {"result" : "success", "sid" : str(sid)}

print str(checkdata ())
