#!/usr/bin/python
import commands,re

import cgi, cgitb
cgitb.enable ()

print "Content-type: text/json"
print

output = commands.getoutput("dispense finger")
lines = output.split("\n")[2:9]

expr = re.compile("^(\d) - (\w+) +(\d+) (.*)")

drinks = {}
for i, line in enumerate (lines):
#	print line
	r = expr.search (line).groups ()
	available = "true"
	if r[1] != "Avail": available = "false"
	drinks["slot" + str(i)] = [r[3].rstrip(), r[2], available]
print str (drinks)
