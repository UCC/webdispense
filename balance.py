#!/usr/bin/python

import commands,re

print "Content-type: text/json"
print
import cgi,cgitb
import os
cgitb.enable ()
import sys, syslog

#import getpass

result = {}
form = cgi.FieldStorage ()

def checkdata ():
	if not (form.has_key("username") and form.has_key("sid")):
		return {"result" : "Missing information!"}
	expr_name = re.compile ("^\w*$")
	username = form["username"].value.lower ()
	sid = form["sid"].value
	if (not expr_name.match (username)):
		return {"result" : "Invalid login!"}
	file = open("sessionids", "r")
        success = False
        for line in file:
                if (":" + username + ":" + sid + "\n") in line:
                        success = True
        file.close()
	if not success:
                log = "Authentication failure for user " + username + " from " + cgi.escape(os.environ["REMOTE_ADDR"]) + "\n"
		syslog.syslog((syslog.LOG_NOTICE | syslog.LOG_AUTH), log)
                #logfile = open("auth.log", "a")
                #logfile.write(log)
                #logfile.close()
                return {"result" : "Authentication Failed"}
	output = commands.getoutput ("dispense acct " + username);
	expr_bal = re.compile ("(?P<bal>\d*\.\d*)")
	balance = expr_bal.search (output).groups()[0]
	
	return {"result" : "success", "balance" : balance}


print str(checkdata ())
